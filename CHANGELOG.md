# Change Log:

## 0.1.2-preview.1

- New Enhancement: added [`SettingsHelpers`](https://github.com/OmiyaGames/omiya-games-global/blob/master/Editor/SettingsHelpers.cs)

## 0.1.1-preview.1

- Updating version of each dependency libraries.
- Updating [`Singleton`](https://github.com/OmiyaGames/omiya-games-global/blob/master/Runtime/Singleton.cs) to compile with latest dependency changes.

## 0.1.0-preview.1

- Initial release
- New Feature: added [`Singleton`](https://github.com/OmiyaGames/omiya-games-global/blob/master/Runtime/Singleton.cs)
- New Feature: added [`ISingletonScript`](https://github.com/OmiyaGames/omiya-games-global/blob/master/Runtime/ISingletonScript.cs)
